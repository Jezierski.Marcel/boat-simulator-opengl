#pragma once

class IslandModel
{
	GLuint VAO;
	GLuint VBO;
	std::vector<GLfloat> island_coord;

public:
	IslandModel(GLfloat size);
	~IslandModel();
	void Draw(const glm::mat4&, GLuint modelLoc) const;
};