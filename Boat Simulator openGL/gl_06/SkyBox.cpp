#define GLEW_STATIC
#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "SkyBox.h"
#include <GLFW/glfw3.h>
using namespace std;

static GLfloat unit_skyBox[] = {
	/*BACK*/
	-0.5f, -0.5f, -0.5f, 0.25f, 0.6666f,
	0.5f, -0.5f, -0.5f, 0.5f, 0.6666f,
	0.5f, 0.5f, -0.5f, 0.5f, 0.3333f,
	0.5f, 0.5f, -0.5f, 0.5f, 0.3333f,
	-0.5f, 0.5f, -0.5f,  0.25f, 0.3333f,
	-0.5f, -0.5f, -0.5f, 0.25f, 0.6666f,

	/*LEFT*/
	-0.5f, 0.5f, 0.5f, 0.0f, 0.3333f,
	-0.5f, 0.5f, -0.5f, 0.25f, 0.3333f,
	-0.5f, -0.5f, -0.5f, 0.25f, 0.6666f,
	-0.5f, -0.5f, -0.5f, 0.25f, 0.6666f,
	-0.5f, -0.5f, 0.5f, 0.0f, 0.6666f,
	-0.5f, 0.5f, 0.5f, 0.0f, 0.3333f,

	/*RIGHT*/
	0.5f, 0.5f, 0.5f, 0.75f, 0.3333f,
	0.5f, 0.5f, -0.5f, 0.5f, 0.3333f,
	0.5f, -0.5f, -0.5f, 0.5f, 0.6666f,
	0.5f, -0.5f, -0.5f, 0.5f, 0.6666f,
	0.5f, -0.5f, 0.5f, 0.75f, 0.6666f,
	0.5f, 0.5f, 0.5f, 0.75f, 0.3333f,

	/*DOWN*/
	-0.5f, -0.5f, -0.5f, 0.25f, 0.66f,
	0.5f, -0.5f, -0.5f, 0.5f, 0.66f,
	0.5f, -0.5f, 0.5f, 0.5f, 1.0f,
	0.5f, -0.5f, 0.5f, 0.5f, 1.0f,
	-0.5f, -0.5f, 0.5f, 0.25f, 1.0f,
	-0.5f, -0.5f, -0.5f, 0.25f, 0.66f,

	/* UP */
	-0.5f, 0.5f, -0.5f, 0.25f, 0.3333f,
	0.5f, 0.5f, -0.5f, 0.5f, 0.3333f,
	0.5f, 0.5f, 0.5f, 0.5f, 0.0f,
	0.5f, 0.5f, 0.5f, 0.5f, 0.0f,
	-0.5f, 0.5f, 0.5f, 0.25f, 0.0f,
	-0.5f, 0.5f, -0.5f, 0.25f, 0.3333f,

	/*FRONT*/
	-0.5f, -0.5f, 0.5f, 1.0f, 0.6666f,
	0.5f, -0.5f, 0.5f, 0.75f, 0.6666f,
	0.5f, 0.5f, 0.5f, 0.75f, 0.3333f,
	0.5f, 0.5f, 0.5f, 0.75f, 0.3333f,
	-0.5f, 0.5f, 0.5f, 1.0f, 0.3333f,
	-0.5f, -0.5f, 0.5f, 1.0f, 0.6666f,

};

SkyBox::SkyBox(GLfloat size)
{
	for (int i = 0; i < _countof(unit_skyBox) / 5; ++i)
	{
		skyBox_coord.push_back(unit_skyBox[i * 5] * size);
		skyBox_coord.push_back(unit_skyBox[i * 5 + 1] * size);
		skyBox_coord.push_back(unit_skyBox[i * 5 + 2] * size);
		skyBox_coord.push_back(unit_skyBox[i * 5 + 3] );
		skyBox_coord.push_back(unit_skyBox[i * 5 + 4] );
	}

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * skyBox_coord.size(), &(skyBox_coord[0]), GL_STATIC_DRAW);

	// vertex geometry data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// vertex texture coordinates
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void SkyBox::Draw(const glm::mat4& modelTrans, GLuint modelLoc) const
{
	glBindVertexArray(VAO);

	// no internal transformations for now
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelTrans));

	glDrawArrays(GL_TRIANGLES, 0, (GLuint)skyBox_coord.size());

	// all is drawn - unbind vertex array
	glBindVertexArray(0);
}

SkyBox::~SkyBox()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}
