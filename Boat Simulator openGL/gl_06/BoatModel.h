#pragma once

class BoatModel
{
	GLuint VAO;
	GLuint VBO;
	std::vector<GLfloat> boat_coord;

public:
	BoatModel(GLfloat size);
	~BoatModel();
	void Draw(const glm::mat4&, GLuint modelLoc) const;
};