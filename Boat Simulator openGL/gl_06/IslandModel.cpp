#define GLEW_STATIC
#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "IslandModel.h"
#include <GLFW/glfw3.h>
using namespace std;

static GLfloat island_triangles[] = {

	/*BOAT down*/
	0.5f, -0.5f, -0.5f, 0.00f, 1.00f,
	0.5f, -0.5f, 0.5f, 1.00f, 0.00f,
	0.7f, -0.5f, 0.0f, 1.00f, 0.50f,


	/*BOAT left front*/
	0.9f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f,

	0.9f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.7f, -0.5f, 0.0f, 0.0f, 0.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f,


	/*BOAT right front*/
	0.9f, 0.5f, 0.0f, 1.0f, 1.0f,
	0.5f, -0.5f, 0.5f, 0.0f, 0.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 1.0f,

	0.9f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.7f, -0.5f, 0.0f, 0.0f, 0.0f,
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f,


};


IslandModel::IslandModel(GLfloat size)
{
	for (int i = 0; i < _countof(island_triangles) / 5; ++i)
	{
		island_coord.push_back(island_triangles[i * 5] * size);
		island_coord.push_back(island_triangles[i * 5 + 1] * size);
		island_coord.push_back(island_triangles[i * 5 + 2] * size);
		island_coord.push_back(island_triangles[i * 5 + 3]);
		island_coord.push_back(island_triangles[i * 5 + 4]);
	}

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * island_coord.size(), &(island_coord[0]), GL_STATIC_DRAW);

	// vertex geometry data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// vertex texture coordinates
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void IslandModel::Draw(const glm::mat4& modelTrans, GLuint modelLoc) const
{
	glBindVertexArray(VAO);

	// no internal transformations for now
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelTrans));

	glDrawArrays(GL_TRIANGLES, 0, (GLuint)island_coord.size());

	// all is drawn - unbind vertex array
	glBindVertexArray(0);
}

IslandModel::~IslandModel()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}
