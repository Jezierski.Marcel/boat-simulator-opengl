#define GLEW_STATIC
#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "BoatModel.h"
#include <GLFW/glfw3.h>
using namespace std;

float x = 0.3f;

static GLfloat boat_triangles[] = {
	/*LEFT*/
	-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f,
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
	-0.5f, 0.5f, -0.5f, 0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,

	/*left*/
	-0.47f, -0.5f + x, -0.45f, 0.0f, 0.0f,
	0.47f, -0.5f + x, -0.45f, 1.0f, 0.0f,
	0.47f, 0.5f, -0.45f, 1.0f, 1.0f,
	0.47f, 0.5f, -0.45f, 1.0f, 1.0f,
	-0.47f, 0.5f, -0.45f, 0.0f, 1.0f,
	-0.47f, -0.5f + x, -0.45f, 0.0f, 0.0f,
	
	/*fat left*/
	0.47f, 0.5f, -0.45f, 0.1f, 0.1f,
	-0.5f, 0.5f, -0.5f, 0.1f, 0.0f,
	0.5f, 0.5f, -0.5f, 0.1f, 0.0f,

	0.47f, 0.5f, -0.45f, 0.1f, 0.1f,
	-0.47f, 0.5f, -0.45f, 0.0f, 0.1f,
	-0.5f, 0.5f, -0.5f, 0.1f, 0.0f,

	/*RIGHT*/
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f,
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
	0.5f, 0.5f, 0.5f, 1.0f, 1.0f,
	0.5f, 0.5f, 0.5f, 1.0f, 1.0f,
	-0.5f, 0.5f, 0.5f, 0.0f, 1.0f,
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f,

	/*right*/
	-0.47f, -0.5f + x, 0.45f, 0.0f, 0.0f,
	0.47f, -0.5f + x, 0.45f, 1.0f, 0.0f,
	0.47f, 0.5f, 0.45f, 1.0f, 1.0f,
	0.47f, 0.5f, 0.45f, 1.0f, 1.0f,
	-0.47f, 0.5f, 0.45f, 0.0f, 1.0f,
	-0.47f, -0.5f + x, 0.45f, 0.0f, 0.0f,

	/*fat right*/
	0.47f, 0.5f, 0.45f, 0.1f, 0.1f,
	-0.5f, 0.5f, 0.5f, 0.1f, 0.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 0.0f,

	0.47f, 0.5f, 0.45f, 0.1f, 0.1f,
	-0.47f, 0.5f, 0.45f, 0.0f, 0.1f,
	-0.5f, 0.5f, 0.5f, 0.1f, 0.0f,

	/*BACK*/
	-0.5f, 0.5f, 0.5f, 1.0f, 1.0f,
	-0.5f, 0.5f, -0.5f, 0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, 1.0f, 0.0f,
	-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
	-0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
	-0.5f, 0.5f, 0.5f, 0.0f, 1.0f,

	/*back*/
	-0.47f, 0.5f, 0.45f, 1.0f, 1.0f,
	-0.47f, 0.5f, -0.45f, 0.0f, 1.0f,
	-0.47f, -0.5f + x, -0.45f, 1.0f, 0.0f,
	-0.47f, -0.5f + x, -0.45f, 0.0f, 0.0f,
	-0.47f, -0.5f + x, 0.45f, 1.0f, 0.0f,
	-0.47f, 0.5f, 0.45f, 0.0f, 1.0f,

	/*fat back*/
	-0.5f, 0.5f, 0.5f, 0.1f, 0.0f,
	-0.5f, 0.5f, -0.5f, 0.0f, 0.0f,
	-0.47f, 0.5f, -0.45f, 0.0f, 0.1f,

	-0.47f, 0.5f, 0.45f, 0.0f, 0.1f,
	-0.47f, 0.5f, -0.45f, 0.1f, 0.1f,
	-0.5f, 0.5f, 0.5f, 0.1f, 0.0f,

	/*BOAT down*/
	0.5f, -0.5f, -0.5f, 0.00f, 1.00f,
	0.5f, -0.5f, 0.5f, 1.00f, 0.00f,
	0.7f, -0.5f, 0.0f, 1.00f, 0.50f,

	/*boat down*/
	0.47f, -0.5f + x, -0.45f, 1.00f, 0.00f,
	0.47f, -0.5f + x, 0.45f, 0.00f, 0.00f,
	0.658f, -0.45f + x, 0.0f, 1.00f, 0.50f,

	/*BOAT left front*/
	0.9f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f,

	0.9f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.7f, -0.5f, 0.0f, 0.0f, 0.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f,

	/*fat left front*/
	0.9f, 0.5f, 0.0f, 0.1f, 0.0f,
	0.5f, 0.5f, -0.5f, 0.0f, 0.0f,
	0.846f, 0.5f, 0.0f, 0.0f, 0.1f,

	0.846f, 0.5f, 0.0f, 0.0f, 0.1f,
	0.47f, 0.5f, -0.45f, 0.1f, 0.1f,
	0.5f, 0.5f, -0.5f, 0.1f, 0.0f,

	/*boat left front*/
	0.846f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.47f, 0.5f, -0.45f, 1.0f, 1.0f,
	0.47f, -0.5f + x, -0.45f, 1.0f, 0.0f,

	0.846f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.658f, -0.45f + x, 0.0f, 0.0f, 0.0f,
	0.47f, -0.5f + x, -0.45f, 1.0f, 0.0f,

	/*BOAT right front*/
	0.9f, 0.5f, 0.0f, 1.0f, 1.0f,
	0.5f, -0.5f, 0.5f, 0.0f, 0.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 1.0f,

	0.9f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.7f, -0.5f, 0.0f, 0.0f, 0.0f,
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f,

	/*fat right front*/

	0.9f, 0.5f, 0.0f, 0.1f, 0.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 0.0f,
	0.846f, 0.5f, 0.0f, 0.0f, 0.1f,

	0.846f, 0.5f, 0.0f, 0.0f, 0.1f,
	0.47f, 0.5f, 0.45f, 0.1f, 0.1f,
	0.5f, 0.5f, 0.5f, 0.1f, 0.0f,

	/*boat right front*/
	0.846f, 0.5f, 0.0f, 1.0f, 1.0f,
	0.47f, -0.5f + x, 0.45f, 0.0f, 0.0f,
	0.47f, 0.5f, 0.45f, 0.0f, 1.0f,

	0.846f, 0.5f, 0.0f, 0.0f, 1.0f,
	0.658f, -0.45f + x, 0.0f, 0.0f, 0.0f,
	0.47f, -0.5f + x, 0.45f, 1.0f, 0.0f,

	/*DOWN*/
	-0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 1.0f,
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f, 0.0f, 1.0f,

	/*down*/
	-0.47f, -0.5f + x, -0.45f, 0.0f, 1.0f,
	0.47f, -0.5f + x, -0.45f, 1.0f, 1.0f,
	0.47f, -0.5f + x, 0.45f, 1.0f, 0.0f,
	0.47f, -0.5f + x, 0.45f, 1.0f, 0.0f,
	-0.47f, -0.5f + x, 0.45f, 0.0f, 0.0f,
	-0.47f, -0.5f + x, -0.45f, 0.0f, 1.0f,

};


BoatModel::BoatModel(GLfloat size)
{
	for (int i = 0; i < _countof(boat_triangles) / 5; ++i)
	{
		boat_coord.push_back(boat_triangles[i * 5] * size);
		boat_coord.push_back(boat_triangles[i * 5 + 1] * size);
		boat_coord.push_back(boat_triangles[i * 5 + 2] * size);
		boat_coord.push_back(boat_triangles[i * 5 + 3]);
		boat_coord.push_back(boat_triangles[i * 5 + 4]);
	}

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * boat_coord.size(), &(boat_coord[0]), GL_STATIC_DRAW);

	// vertex geometry data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// vertex texture coordinates
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void BoatModel::Draw(const glm::mat4& modelTrans, GLuint modelLoc) const
{
	glBindVertexArray(VAO);

	// no internal transformations for now
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelTrans));

	glDrawArrays(GL_TRIANGLES, 0, (GLuint)boat_coord.size());

	// all is drawn - unbind vertex array
	glBindVertexArray(0);
}

BoatModel::~BoatModel()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}
