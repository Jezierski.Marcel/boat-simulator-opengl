#define GLEW_STATIC
#include <GL/glew.h>
#include "shprogram.h"
#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <iostream>
#include <vector>
using namespace std;
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "CubeModel.h"
#include "BoatModel.h"
#include "SkyBox.h"
#include "IslandModel.h"

const GLuint WIDTH = 800, HEIGHT = 600;

// key buffer for camera movement
bool keys[1024];

float boatX = 0.0f;
float boatZ = 0.0f;
float velocity = 0.2f;

float rotateBoat = 0.0f;
float rotSens = 1.0f;

float rudderRot = 0.0f;

int v1, v2 =0;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	cout << key << endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			keys[key] = true;
		else if (action == GLFW_RELEASE)
			keys[key] = false;
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		rotateBoat += rotSens*rudderRot*0.03f;
		if (rotateBoat == 360.0f) rotateBoat = 0.0f;
		boatX += cos(glm::radians(rotateBoat))*velocity;
		boatZ -= sin(glm::radians(rotateBoat))*velocity;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		rotateBoat -= rotSens*rudderRot*0.02f;
		if (rotateBoat == -360.0f) rotateBoat = 0.0f;
		boatX -= cos(glm::radians(rotateBoat))*velocity;
		boatZ += sin(glm::radians(rotateBoat))*velocity;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		rudderRot += rotSens;
		if (rudderRot > 75.0f) rudderRot = 80.0f;

	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
		rudderRot -= rotSens;
		if (rudderRot < -75.0f) rudderRot = -80.0f;
	}
}

// camera
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

bool firstMouse = true;
float yaw = -90.0f;	// yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
float pitch = 0.0f;
float lastX = 800.0f / 2.0;
float lastY = 600.0 / 2.0;
float fov = 45.0f;

// Deltatime
GLfloat deltaTime = 0.0f;	// Time between current frame and last frame
GLfloat lastFrame = 0.0f;  	// Time of last frame


void processInput(GLFWwindow *window)
{
	float cameraSpeed = 2.5f * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		cameraPos += cameraSpeed * cameraFront;
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		cameraPos -= cameraSpeed * cameraFront;
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}

GLuint LoadMipmapTexture(GLuint texId, const char* fname)
{
	int width, height;
	unsigned char* image = SOIL_load_image(fname, &width, &height, 0, SOIL_LOAD_RGB);
	if (image == nullptr)
		throw exception("Failed to load texture file");

	GLuint texture;
	glGenTextures(1, &texture);

	glActiveTexture(texId);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);
	return texture;
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.1f; // change this value to your liking
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	// make sure that when pitch is out of bounds, screen doesn't get flipped
	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	cameraFront = glm::normalize(front);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	if (fov >= 1.0f && fov <= 45.0f)
		fov -= yoffset;
	if (fov <= 1.0f)
		fov = 1.0f;
	if (fov >= 45.0f)
		fov = 45.0f;
}

int main()
{
	if (glfwInit() != GL_TRUE)
	{
		cout << "GLFW initialization failed" << endl;
		return -1;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	try
	{
		GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Boat", nullptr, nullptr);
		if (window == nullptr)
			throw exception("GLFW window not created");
		glfwMakeContextCurrent(window);
		glfwSetKeyCallback(window, key_callback);
		glfwSetCursorPosCallback(window, mouse_callback);
		glfwSetScrollCallback(window, scroll_callback);

		// tell GLFW to capture our mouse
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
			throw exception("GLEW Initialization failed");

		glViewport(0, 0, WIDTH, HEIGHT);

		// Build, compile and link shader program

		ShaderProgram theProgram("gl_06.vert", "gl_06.frag");

		// Set up models
		CubeModel water(1.0f, 1000.0f);
		CubeModel rudder(3.0f, 1.0f);
		IslandModel island(5.0f);
		BoatModel boat(7.0f);
		SkyBox skybox(256.0f);

		// Set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// Set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// prepare textures
		GLuint texture0 = LoadMipmapTexture(GL_TEXTURE0, "skybox3.png");
		GLuint texture1 = LoadMipmapTexture(GL_TEXTURE1, "water.png");
		GLuint texture2 = LoadMipmapTexture(GL_TEXTURE2, "boat_material.png");
		GLuint texture3 = LoadMipmapTexture(GL_TEXTURE3, "rock.jpg");

		glEnable(GL_DEPTH_TEST); // let's use z-buffer

		// main event loop
		while (!glfwWindowShouldClose(window))
		{
			// Calculate deltatime of current frame
			GLfloat currentFrame = (GLfloat) glfwGetTime();
			deltaTime = currentFrame - lastFrame;
			lastFrame = currentFrame;

			// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
			glfwPollEvents();
			processInput(window);
//			do_movement();

			// Clear the colorbuffer
			glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			// Bind Textures using texture units
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture0);
			//glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 0);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, texture1);
			//glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 1);
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, texture2);
			//glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 2);
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, texture3);
			//glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 3);

			theProgram.Use();
			GLuint modelLoc = glGetUniformLocation(theProgram.get_programID(), "model");
			GLuint viewLoc = glGetUniformLocation(theProgram.get_programID(), "view");
			GLuint projLoc = glGetUniformLocation(theProgram.get_programID(), "projection");
			
			// set perspective projection & pass it to GPU
			glm::mat4 projection = glm::perspective(glm::radians(2*fov), (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 300.0f);
//			glm::mat4 projection = glm::perspective(glm::radians(45.0f), (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);
			glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

			// set view transformation & pass it to GPU
			glm::mat4 view;
//			float radius = 3.0f;
//			float camX = sin(glfwGetTime()) * radius;
//			float camZ = cos(glfwGetTime()) * radius;
//			view = glm::lookAt(glm::vec3(camX, 0.0, camZ), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));
			view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			
//====================================DRAW WATER=========================================================
			glm::mat4 trans;
			trans = glm::translate(trans, glm::vec3(0.0f, -4.0f, 0.0f));
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(trans));
			trans = glm::scale(trans, glm::vec3(10000.0f, 7.0f, 10000.0f));
			glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 1);
			water.Draw(trans, modelLoc);
//================================DRAW BOAT==================================================================
			trans = glm::mat4();
			trans = glm::translate(trans, glm::vec3(boatX, 0.0f, boatZ));
			trans = glm::translate(trans, glm::vec3(+3.0f, 0.0f, 0.0f));
			trans = glm::rotate(trans, glm::radians(rotateBoat), glm::vec3(0.0f, 1.0f, 0.0f));
			trans = glm::translate(trans, glm::vec3(-3.0f, 0.0f, 0.0f));
			//trans = glm::translate(trans, glm::vec3(boatX, 0.0f, boatZ));
			trans = glm::scale(trans, glm::vec3(1.0f, 0.3f, 0.6f));
			glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 2);
			boat.Draw(trans, modelLoc);
//=================================DRAW RUDDER==============================================================
			
			trans = glm::mat4();
			trans = glm::translate(trans, glm::vec3(boatX , 0.0f, boatZ));
			trans = glm::translate(trans, glm::vec3(+3.0f, 0.0f, 0.0f));
			trans = glm::rotate(trans, glm::radians(rotateBoat), glm::vec3(0.0f, 1.0f, 0.0f));
			trans = glm::translate(trans, glm::vec3(-3.0f, 0.0f, 0.0f));

			trans = glm::translate(trans, glm::vec3(-3.5f, 0.0f, 0.0f));
			trans = glm::rotate(trans, glm::radians(-rudderRot), glm::vec3(0.0f, 1.0f, 0.0f));
			trans = glm::translate(trans, glm::vec3(+3.5f, 0.0f, 0.0f));

			trans = glm::translate(trans, glm::vec3(- 4.25f, -0.4f, 0.0f));
			trans = glm::scale(trans, glm::vec3(0.5f, 1.1f, 0.03f));
			rudder.Draw(trans, modelLoc);

//====================DRAW SKYBOX=====================================================================
			trans = glm::mat4();
			trans = glm::translate(trans, glm::vec3(cameraPos));
			glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 0);
			skybox.Draw(trans, modelLoc);
//==========================DRAW ISLAND=====================================================================
			trans = glm::mat4();
			trans = glm::rotate(trans, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
			trans = glm::translate(trans, glm::vec3(-20.0f, -0.0f, 20.0f));
			trans = glm::scale(trans, glm::vec3(7.5f, 1.0f, 5.0f));
			glUniform1i(glGetUniformLocation(theProgram.get_programID(), "Texture0"), 3);
			island.Draw(trans, modelLoc);
			trans = glm::translate(trans, glm::vec3(5.0f, -0.0f, 0.0f));
			trans = glm::rotate(trans, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			island.Draw(trans, modelLoc);
//================================================================================================================

			// Swap the screen buffers
			glfwSwapBuffers(window);
		}
	}
	catch (exception ex)
	{
		cout << ex.what() << endl;
	}
	glfwTerminate();
	return 0;
}
