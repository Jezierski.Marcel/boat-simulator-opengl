#pragma once

class SkyBox
{
	GLuint VAO;
	GLuint VBO;
	std::vector<GLfloat> skyBox_coord;

public:
	SkyBox(GLfloat size);
	~SkyBox();
	void Draw(const glm::mat4&, GLuint modelLoc) const;
};

