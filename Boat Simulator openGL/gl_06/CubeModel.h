#pragma once

class CubeModel
{
	GLuint VAO;
	GLuint VBO;
	std::vector<GLfloat> cube_coord;

public:
	CubeModel(GLfloat size, GLfloat tex);
	~CubeModel();
	void Draw(const glm::mat4&, GLuint modelLoc) const;
};

